package edu.ntnu.idatt2003;

public enum EnumSingleton {
    INSTANCE;

    public static void main(String[] args) {
        System.out.println(EnumSingleton.INSTANCE == EnumSingleton.INSTANCE);
    }
}
