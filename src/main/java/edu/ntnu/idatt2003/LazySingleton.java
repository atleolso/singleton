package edu.ntnu.idatt2003;

public class LazySingleton {

    private static LazySingleton instance;

    private LazySingleton() {
    }

    public static LazySingleton getInstance() {
        if (instance == null) {
            instance = new LazySingleton();
        }
        return instance;
    }

    public static void main(String[] args) {
        System.out.println(LazySingleton.getInstance() == LazySingleton.getInstance());
    }
}
